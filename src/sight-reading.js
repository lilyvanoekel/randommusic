"use strict";

const Util = require('./Library/Util.js');
const Defines = require('./Library/Defines.js');

const Progression = require('./Library/Progression.js');
const Scale = require('./Library/Scale.js');
const Song = require('./Library/Song.js');
const SongToAbc = require('./Library/Song/SongToAbc.js');
const Voice = require('./Library/Voice.js');
const Melody = require('./Library/Melody.js');
const Note = require('./Library/Note.js');
const Rhythm = require('./Library/Rhythm.js');
const RandomNoteArray = require('./Library/Melody/RandomNoteArray.js');
const SimpleLeftHandArpeggio = require('./Library/Melody/SimpleLeftHandArpeggio.js');

let op = function(field, options, defaultValue) {
    if (
        typeof(options) == 'undefined'
        || typeof(options[field]) == 'undefined'
    ) {
        return defaultValue;
    }
    
    return options[field];
};

let generateAbc = function(options) {
    const bpm = op('bpm', options, 120);
    const structure = op('structure', options, 'AABA');
    const mode = op('mode', options, 'major');
    const key = op('key', options, 'C');
    const time = op('time', options, '4');
    const lessChordNotes = op('lessChordNotes', options, false);
    const randomRhythm = op('randomRhythm', options, true);
    
    let scale = Scale.create(key, mode);
    const title = op('title', options, 'Sight-Reading Exercise - ' + scale.display);
    
    let timeInt = parseInt(time, 10);
    let ticksPerMeasure = timeInt * Defines.ticksPerQuarter;
    let progression = Progression.createRandomFourChord(scale, ticksPerMeasure);
    
    let rangeStart = 49;
    let rangeEnd = 81;
    let measures = progression.length;
    let wrapLow = 59;
    let notesInBetween = 3;
    let repeats = 1;
    let melodyChordNotes = Defines.ticksPerQuarter;
    
    if (lessChordNotes) {
        notesInBetween = 7;
        melodyChordNotes *= 2;
        
        if (time == '3') {
            notesInBetween = 5;
            melodyChordNotes = Defines.ticksPerQuarter * 1.5;
        }
    }
    
    let noteArray = RandomNoteArray.create(scale, progression, melodyChordNotes, notesInBetween, rangeStart, rangeEnd, wrapLow);
    let arpeggioArray = SimpleLeftHandArpeggio.create(scale, progression, time);
    
    let rhythm1;
    let rhythm2;
    
    if (randomRhythm) {
        rhythm1 = Rhythm.getRandomRhythm(measures, ticksPerMeasure, 48);
        rhythm2 = Rhythm.getRandomRhythm(measures, ticksPerMeasure, 48);
    } else {
        rhythm1 = Rhythm.getXRandomPredefinedRhythm8(measures, time);
        rhythm2 = Rhythm.getXRandomPredefinedRhythm8(measures, time);
    }
    
    let voiceRightHand = Voice.create('treble');
    let melody1 = Melody.combineNoteArrayRhythm(noteArray, rhythm1, Defines.ticksPer16th);
    voiceRightHand.addMelody(
        melody1,
        repeats
    );
    
    let voiceLeftHand = Voice.create('bass');
    voiceLeftHand.addMelody(
        Melody.combineNoteArrayRhythm(arpeggioArray, Rhythm.getXSimpleQuarterNotes(measures, time), Defines.ticksPerQuarter),
        repeats
    );
    
    if (structure == 'AABA') {
        noteArray.generate();
        voiceRightHand.addMelody(
            Melody.combineNoteArrayRhythm(noteArray, rhythm2, Defines.ticksPer16th)
        );
        
        voiceRightHand.addMelody(melody1);
        
        voiceLeftHand.addMelody(
            Melody.combineNoteArrayRhythm(arpeggioArray, Rhythm.getXSimpleQuarterNotes(measures, time), Defines.ticksPerQuarter)
        );
        
        voiceLeftHand.addMelody(
            Melody.combineNoteArrayRhythm(arpeggioArray, Rhythm.getXSimpleQuarterNotes(measures, time), Defines.ticksPerQuarter)
        );
    };
    
    voiceRightHand.addEndNote(
        Note.create(voiceRightHand.measures[0].notes[0].pitch, timeInt * Defines.ticksPerQuarter)
    );
    
    voiceLeftHand.addEndNote(
        Note.create(voiceLeftHand.measures[0].notes[0].pitch, timeInt * Defines.ticksPerQuarter)
    );
    
    for (let i=0; i<progression.length; i++) {
        voiceRightHand.measures[i].notes[0].txt = progression.chords[i];
        
        if (structure == 'AABA') {
            voiceRightHand.measures[i + 4].notes[0].txt = progression.chords[i];
            voiceRightHand.measures[i + 8].notes[0].txt = progression.chords[i];
        }
    }
    
    let song = Song.create(scale, time, bpm, title);
    song.addVoice(voiceRightHand);
    song.addVoice(voiceLeftHand);
    
    return SongToAbc.convert(song);
};

if (typeof(window) != 'undefined') {
    window.generateAbc = generateAbc;
}

module.exports = generateAbc;

/*console.log(generateAbc({
    'bpm': 120, 
    'structure': 'AABA', 
    'mode': 'minor', 
    'key': 'C', 
    'time': '3'
}));*/