 "use strict";

const KeyOffset = require('./Data/KeyOffset.js');
const ModeOffset = require('./Data/ModeOffset.js');

class Scale {
    constructor(key, mode) {
        this.key = key;
        this.mode = mode;
    }
    
    get notes() {
        //                C  D  E  F  G  A  B
        let majorScale = [0, 2, 4, 5, 7, 9, 11];
        let offset = KeyOffset[this.key] + ModeOffset[this.mode];
        
        for (let i=0; i<majorScale.length; i++) {
            majorScale[i] = (majorScale[i] + offset) % 12;
        }
        
        return majorScale;
    }
    
    get display() {
        const modeText = {
            'major': 'Major',
            'minor': 'Minor',
            'mix': 'Mixolydian',
            'loc': 'Locrian',
            'dor': 'Dorian',
            'phr': 'Phrygian',
            'lyd': 'Lydian'
        };
        
        return this.key + ' ' + modeText[this.mode];
    }
}

module.exports = {
    create: function(key, mode) {
        return new Scale(key, mode);
    }
};