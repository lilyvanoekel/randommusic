module.exports = {
    ticksPerQuarter: 96,
    ticksPer16th: 24,
    defaultTicksPerBar: 384
};