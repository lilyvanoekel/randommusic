"use strict";

class Song {
    constructor(scale, time, bpm, title) {
        this.scale = scale;
        this.time = time;
        this.bpm = bpm;
        this.title = title;
        this.voices = [];
    }
    
    addVoice(voice) {
        this.voices.push(voice);
    }
}

module.exports = {
    create: function(scale, time, bpm, title) {
        return new Song(scale, time, bpm, title);
    }
};