module.exports = {
    arrayRandomChild: function(array) {
        return array[Math.floor(Math.random() * array.length)];
    },

    getXRandomFromArray: function(arr, n) {
        let result = new Array(n),
            len = arr.length,
            taken = new Array(len);
        
        if (n > len) {
            //throw new RangeError("getRandom: more elements taken than available");
            while (n--) {
                let x = Math.floor(Math.random() * len);
                result[n] = arr[x in taken ? taken[x] : x];
            }
            
            return result;
        }
        
        while (n--) {
            let x = Math.floor(Math.random() * len);
            result[n] = arr[x in taken ? taken[x] : x];
            taken[x] = --len;
        }
        
        return result;
    },
    
    difference: function (a, b) {
        return Math.abs(a - b);
    }
};