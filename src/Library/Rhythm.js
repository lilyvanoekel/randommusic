"use strict";

const Util = require('./Util.js');
const PredefinedRhythm8 = require('./Data/PredefinedRhythm8.js');

let randomlySplitSome8NotesTo16 = function(r) {
    var result = [];
    
    // 50% chance to do nothing
    if (Util.arrayRandomChild([false, true])) {
        return r;
    }
    
    let number8Notes = 0;
    let indexes = [];
    for (let i=0; i<r.length; i++) {
        if (r[i] == 48) {
            indexes.push(i);
            number8Notes++;
        }
    }
    
    if (!indexes.length) {
        return r;
    }
    
    let howManyToAffect = Math.floor(Math.random() * (number8Notes - 1)) + 1;
    
    if (howManyToAffect > 6) {
        howManyToAffect = Math.floor(Math.random() * (number8Notes - 1)) + 1;
    }

    if (howManyToAffect > 3) {
        howManyToAffect = Math.floor(Math.random() * (number8Notes - 1)) + 1;
    }       
    
    let indexesToAffect = Util.getXRandomFromArray(indexes, howManyToAffect);
    
    for (let i=0; i<r.length; i++) {
        if (indexesToAffect.indexOf(i) != -1) {
            result.push(24);
            result.push(24);
        } else {
            result.push(r[i]);
        }
    }
    
    return result;
};

module.exports = {
    getXRandomPredefinedRhythm8: function(howMany, time) {
        let r = [];
        
        for (let i=0; i<howMany; i++) {
            r.push(Util.arrayRandomChild(PredefinedRhythm8[time]));
        }
        
        return r;
    },
    
    getXSimpleQuarterNotes: function(howMany, time) {
        let one = (time == '3' ? [96, 96, 96] : [96, 96, 96, 96]);
        let r = [];
        
        for (let i=0; i<howMany; i++) {
            r.push(one);
        }
        
        return r;
    },
    
    getRandomRhythm: function(howMany, ticksPerMeasure, smallestNote) {
        const validLength = [48, 96, 144, 192, 288, 384];
        
        let getNextValidLengthValue = function(value) {
            let validIndex = validLength.indexOf(value);
            validIndex++;
            return validLength[validIndex];
        }
        
        let nextLength = function(r, index) {
            return getNextValidLengthValue(r[index]);
        };
        
        let randomRhythm = function(ticksPerMeasure, smallestNote) {
            let min = 1;
            let max = Math.floor(ticksPerMeasure / smallestNote) + 1;
            let numberOfNotes = Math.floor(Math.random() * (max - min)) + min;
            
            if (numberOfNotes > 7 || numberOfNotes == 1) {
                numberOfNotes = Math.floor(Math.random() * (max - min)) + min;
            }
            
            let r = [];
            let total = 0;
            
            for (let i=0; i<numberOfNotes; i++) {
                r.push(smallestNote);
                total += smallestNote;
            }
            
            while (total < ticksPerMeasure) {
                let indexToUpdate = Math.floor(Math.random() * numberOfNotes);
                
                while ((total - r[indexToUpdate] + getNextValidLengthValue(r[indexToUpdate])) > ticksPerMeasure) {
                    indexToUpdate = Math.floor(Math.random() * numberOfNotes);
                }
                
                total = total - r[indexToUpdate] + getNextValidLengthValue(r[indexToUpdate]);
                r[indexToUpdate] = getNextValidLengthValue(r[indexToUpdate]);
            }
            
            return r;
        };
        
        let r = [];
        
        for (let i=0; i<howMany; i++) {
            r.push(randomRhythm(ticksPerMeasure, smallestNote));
        }
        
        return r;
    }
};