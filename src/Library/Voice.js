"use strict";

const Measure = require('./Measure.js');
const Note = require('./Note.js');

class Voice {
    constructor(clef) {
        this.clef = clef;
        this.title = '';
        this.measures = [];
    }
    
    addMelody(melody, repeat) {
        repeat = (typeof(repeat) == 'undefined' ? 0 : repeat);
        
        let lastMeasure = -1;
        let currentMeasure = false;
        
        for (let i=0; i<melody.length; i++) {
            if (lastMeasure != melody[i]['measure']) {
                lastMeasure = melody[i]['measure'];
                currentMeasure = Measure.create();
                this.measures.push(currentMeasure);
                
                if (!i && repeat) {
                    currentMeasure.repeatStart = repeat;
                }
            }
            
            currentMeasure.notes.push(
                Note.create(melody[i]['note'], melody[i]['duration'])
            );
        }
        
        if (repeat) {
            currentMeasure.repeatEnd = repeat;
        }
    }
    
    addEndNote(note) {
        let measure = Measure.create();
        measure.notes.push(note);
        this.measures.push(measure);
    }
}

module.exports = {
    create: function(clef) {
        return new Voice(clef);
    }
};