"use strict";

class Note {
    constructor(pitch, duration) {
        this.pitch = pitch;
        this.duration = duration;
        this.isRest = false;
        this.txt = '';
        this.tie = false;
    }
    
    copy() {
        return Object.assign({}, this);
    }
}

module.exports = {
    create: function(pitch, duration) {
        return new Note(pitch, duration);
    }
};