"use strict";

module.exports = {
    combineNoteArrayRhythm: function(noteArray, rhythmArray, noteResolution) {
        let melody = [];
        
        let skip = 0;
        let noteInMeasure = 0;
        let localRhythm = false;
        let measure = -1;
        
        let totalInRhythm = rhythmArray[0].reduce(function(total, num) {
            return total + num;
        });
        
        totalInRhythm /= noteResolution;
        
        for (let i=0; i<noteArray.notes.length; i++) {
            if (skip) {
                skip--;
                continue;
            }
            
            if (!(i%totalInRhythm)) {
                noteInMeasure = 0;
                measure++;
            }
            
            let note = noteArray.notes[i];
            
            let character = note;
            let noteLength = rhythmArray[Math.floor(i / totalInRhythm)][noteInMeasure];
            
            noteInMeasure++;
            skip = (noteLength / noteResolution) - 1;
            
            melody.push({
                note: note,
                duration: noteLength,
                measure: measure
            });
        }
        
        return melody;
    }
};