"use strict";

const Util = require('./Util.js');
const Defines = require('./Defines.js');
const Predefined4Chord = require('./Data/FourChordProgressions.js');
const ModeChords = require('./Data/ModeChords.js');
const KeyOffset = require('./Data/KeyOffset.js');
const ModeOffset = require('./Data/ModeOffset.js');

class Progression {
    constructor(scale) {
        this.scale = scale;
        this.chords = [];
    }
    
    set chords(chords) {
        this._chords = chords;
        this.length = this._chords.length;
    }
    
    get chords() {
        return this._chords;
    }
    
    get chordNotes() {
        let notesInChords = [];
        let offset = KeyOffset[this.scale.key] + ModeOffset[this.scale.mode];
        
        for (let i=0; i<this.chords.length; i++) {
            let chord = [];
            
            for (let j=0; j<ModeChords[this.scale.mode][this.chords[i]].length; j++) {
                chord[j] = (ModeChords[this.scale.mode][this.chords[i]][j] + offset) % 12;
            }
            
            notesInChords.push(chord);
        }
        
        return notesInChords;
    }
    
    set duration(duration) {
        if (typeof(duration.length) != 'undefined') {
            this._duration = duration;
            return;
        }
        
        this._duration = [];
        
        for (let i=0; i<this.length; i++) {
            this._duration.push(duration);
        }
    }
    
    get duration() {
        return this._duration;
    }
}

module.exports = {
    create: function(scale, chords) {
        return new Progression(scale, chords);
    },
    
    createRandomFourChord: function(scale, duration) {
        duration = (typeof(duration) == 'undefined' ? Defines.defaultTicksPerBar : duration);
        
        let progression = new Progression(scale);
        progression.chords = Util.arrayRandomChild(Predefined4Chord[scale.mode]);
        progression.duration = duration;
        return progression;
    }
};