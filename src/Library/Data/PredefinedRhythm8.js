"use strict";

let rhythm = {
    '3': [
        [4, 2, 2, 2, 2],
        [6, 6],
        [4, 4, 4],
        [2, 4, 2, 4],
        [6, 2, 2, 2],
        [8, 2, 2],
        [8, 4],
        [4, 2, 4, 2],
        [4, 2, 2, 4],
        [2, 4, 6],
        [4, 2, 6],
        [2, 2, 4, 2, 2]
    ],
    '4': [
        [4, 4, 4, 4],
        [4, 4, 2, 2, 2, 2],
        [4, 2, 4, 2, 4],
        //[4, 2, 2, 2, 2, 4],
        [2, 2, 4, 2, 2, 4],
        [2, 4, 2, 4, 2, 2],
        [4, 4, 6, 2],
        [6, 2, 6, 2],
        [2, 2, 2, 2, 4, 4],
        [6, 6, 4]
    ]
};

for (let time in rhythm) {
    for (let i=0; i<rhythm[time].length; i++) {
        for (let j=0; j<rhythm[time][i].length; j++) {
            rhythm[time][i][j] = rhythm[time][i][j] * 24;
        }
    }
}

module.exports = rhythm;