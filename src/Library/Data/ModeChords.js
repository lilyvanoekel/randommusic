const chords = {
    'cmajor': [0, 4, 7],
    'dminor': [2, 5, 9],
    'eminor': [4, 7, 11],
    'fmajor': [5, 9, 0],
    'gmajor': [7, 11, 2],
    'aminor': [9, 0, 4],
    'bdim': [11, 2, 5]
};

module.exports = {
    'major': {
        'I': chords['cmajor'],
        'ii': chords['dminor'],
        'iii': chords['eminor'],
        'IV': chords['fmajor'],
        'V': chords['gmajor'],
        'vi': chords['aminor'],
        'vii°': chords['bdim']
    },
    'minor': {
        'i': chords['aminor'],
        'ii°': chords['bdim'],
        'III': chords['cmajor'],
        'iv': chords['dminor'],
        'v': chords['eminor'],
        'v7': [4, 7, 11, 2],
        'VI': chords['fmajor'],
        'VII': chords['gmajor']
    },
    'mix': {
        'I': chords['gmajor'],
        'ii': chords['aminor'],
        'iii°': chords['bdim'],
        'IV': chords['cmajor'],
        'v': chords['dminor'],
        'vi': chords['eminor'],
        'VII': chords['fmajor']
    },
    'dor': {
        'i': chords['dminor'],
        'ii': chords['eminor'],
        'III': chords['fmajor'],
        'IV': chords['gmajor'],
        'v': chords['aminor'],
        'vi°': chords['bdim'],
        'VII': chords['cmajor']
    },
    'phr': {
        'i': chords['eminor'],
        'II': chords['fmajor'],
        'III': chords['gmajor'],
        'iv': chords['aminor'],
        'v°': chords['bdim'],
        'VI': chords['cmajor'],
        'vii': chords['dminor']
    },
    'lyd': {
        'I': chords['fmajor'],
        'II': chords['gmajor'],
        'iii': chords['aminor'],
        'iv°': chords['bdim'],
        'V': chords['cmajor'],
        'vi': chords['dminor'],
        'vii': chords['eminor']
    },
    'loc': {
        'i°': chords['bdim'],
        'II': chords['cmajor'],
        'iii': chords['dminor'],
        'iv': chords['eminor'],
        'V': chords['fmajor'],
        'VI': chords['gmajor'],
        'vii': chords['aminor']
    }
};