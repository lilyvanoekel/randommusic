module.exports = {
    'major': [
        ['I', 'V', 'vi', 'IV'],
        ['I', 'vi', 'IV', 'V'],
        ['I', 'vi', 'ii', 'V'],
        ['I', 'IV', 'vi', 'V'],
        ['I', 'iii', 'IV', 'V'],
        ['I', 'IV', 'I', 'V'],
        ['I', 'IV', 'ii', 'V']
    ],
    'minor': [
        ['i', 'VI', 'III', 'VII'],
        ['i', 'iv', 'v', 'i'],
        ['VI', 'VII', 'i', 'i'],
        ['i', 'VII', 'VI', 'VII'],
        ['i', 'i', 'iv', 'VII']/*,
        ['i', 'iv', 'v7', 'i']*/
    ],
    'mix': [
        ['I', 'IV', 'v', 'I'],
        ['v', 'ii', 'IV', 'I'],
        ['I', 'IV', 'I', 'VII'],
        ['I', 'ii', 'IV', 'VII']
    ],
    'loc': [
        ['i°', 'iii', 'vii', 'i°']
    ],
    'dor': [
        ['i', 'IV', 'i', 'VII'],
        ['i', 'IV', 'ii', 'v'],
        ['i', 'III', 'IV', 'i']
    ],
    'phr': [
        ['i', 'II', 'III', 'i'],
        // ['i', 'V7', 'i', 'II'],
        ['i', 'iv', 'i', 'II'],
        ['i', 'III', 'vii', 'i']
    ],
    'lyd': [
        ['I', 'II', 'iv°', 'V'],
        ['I', 'vi', 'II', 'V'],
        ['I', 'V', 'I', 'II']
    ]
};