"use strict";

const keysFlats = require('../Data/KeysFlats.js');

let noteToLetter = function(scale, note) {
    let notes = ['c', 'c', 'd', 'd', 'e', 'f', 'f', 'g', 'g', 'a', 'a', 'b'];
    
    if (keysFlats.indexOf(scale.key + ' ' + scale.mode) != -1) {
        notes = ['c', 'd', 'd', 'e', 'e', 'f', 'g', 'g', 'a', 'a', 'b', 'b'];
    }
    
    return notes[note % 12];
};

let noteToCharacter = function(scale, note) {
    let octave = Math.floor(note.pitch / 12);
    let character = noteToLetter(scale, note.pitch)
    
    switch(octave) {
        case 1:
        case 2:
        case 3:
            character = character.toUpperCase() + ',,';
            break;
        case 4:
            character = character.toUpperCase() + ',';
            break;
        case 5:
            character = character.toUpperCase();
            break;
        case 6:
            break;
        case 7:
        case 8:
            character += '\'';
            break;
    }
    
    return character;
};

module.exports = {
    convert: function(song) {
        let output = "X:1\nT:" + song.title + "\nC:gimmesomethingtoplay.com\nM:" + song.time + "/4\nK:" + song.scale.key + " " + song.scale.mode + "\nQ:1/4 = " + song.bpm + "\n";
        
        for (let i=0; i<song.voices.length; i++) {
            output += 'V:P' + (i + 1) + ' clef=' + song.voices[i].clef + "\n";
            output += "L:1/16\n";
        }
        
        for (let i=0; i<song.voices.length; i++) {
            output += '[V:P' + (i + 1) + ']|';
            
            for (let j=0; j<song.voices[i].measures.length; j++) {
                let measure = song.voices[i].measures[j];
                measure.fixSyncopation();
                
                if (j && !(j % 4) && j < (song.voices[i].measures.length - 1)) {
                    output += "\n";
                    output += '[V:P' + (i + 1) + ']|';
                }
                
                if (measure.repeatStart) {
                    output += ':';
                }
                
                let position = 0;
                let lastWas8th = false;
                
                measure.notes.forEach((note, index) => {
                    if (note.txt) {
                        output += '"' + note.txt + '"';
                    }
                    
                    let addSpace = true;
                    
                    if (!index || (note.duration == 48 && lastWas8th && position % 96 == 48)) {
                        addSpace = false;
                    }
                    
                    output += (addSpace ? ' ' : '');
                    output += (note.tie ? '-' : '');
                    output += noteToCharacter(song.scale, note);
                    output += (note.duration / 24);
                    position += note.duration;
                    lastWas8th = note.duration == 48;
                });
                
                if (measure.repeatEnd) {
                    output += ':';
                }
                
                output += '|';
            }
            
            output += "\n";
        }
        
        return output;
    }
};