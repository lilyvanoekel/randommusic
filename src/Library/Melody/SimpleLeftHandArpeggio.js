"use strict";

const Util = require('../Util.js');

class SimpleArpeggio {
    constructor(scale, progression, time) {
        this.scale = scale;
        this.progression = progression;
        this.time = time;
        this.notes = [];
    }
    
    generate() {
        this.notes = [];
        
        for (let i=0; i<this.progression.chordNotes.length; i++) {
            let chordNotes = [];
            let highest = 0;
            let lowest = 128;
            
            for (let j=0; j<3; j++) {
                chordNotes[j] = this.progression.chordNotes[i][j];
                
                if (j && chordNotes[j] < chordNotes[0]) {
                    chordNotes[j] += 12;
                }
                
                if (this.time != '3' && j == 2 && Util.arrayRandomChild([0, 1])) {
                    chordNotes[j] -= 12;
                }
                
                if (chordNotes[j] > highest) {
                    highest = chordNotes[j];
                }
                
                if (chordNotes[j] < lowest) {
                    lowest = chordNotes[j];
                }
            }
            
            let amountToAdd = 36;
            if (highest < 12) {
                amountToAdd = Util.arrayRandomChild([0, 1]) ? 48 : 36;
            } else {
                amountToAdd = Util.arrayRandomChild([0, 1]) ? 24 : 36;
            }
            
            if (this.time == '3' && amountToAdd == 24 && lowest < 36) {
                amountToAdd = 36;
            }
            
            this.notes.push(chordNotes[0] + amountToAdd);
            this.notes.push(chordNotes[1] + amountToAdd);
            this.notes.push(chordNotes[2] + amountToAdd);
            
            if (this.time != '3') {
                this.notes.push(chordNotes[1] + amountToAdd);
            }
        }
        
        return this;
    }
}

module.exports = {
    create: function(scale, progression, time) {
        let simpleArpeggio = new SimpleArpeggio(scale, progression, time);
        return simpleArpeggio.generate();
    }
}