"use strict";

const Util = require('../Util.js');

class RandomNoteArray {
    constructor(scale, progression, chordNoteLength, notesBetween, rangeStart, rangeEnd, wrapLow) {
        this.scale = scale;
        this.progression = progression;
        this.chordNoteLength = chordNoteLength;
        this.notesBetween = notesBetween;
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.wrapLow = wrapLow;
        this.notes = [];
    }
    
    validNotes(note) {
        let valid = [];
        
        for (let i=this.rangeStart; i<=this.rangeEnd; i++) {
            if (i % 12 == note) {
                valid.push(i);
            }
        }
        
        return valid;
    }
    
    pickNote(chord, previousNote) {
        let allValid = [];
        for (let i=0; i<chord.length; i++) {
            allValid = allValid.concat(this.validNotes(chord[i]));
        }
        
        if (typeof(previousNote) == 'undefined') {
            return Util.arrayRandomChild(allValid);
        }
        
        let chosen = Util.arrayRandomChild(allValid)
        
        while (Util.difference(chosen, previousNote) > 12) {
            chosen = Util.arrayRandomChild(allValid)
        }
        
        return chosen;
    }
    
    noteInScale(note, scale) {
        note = note % 12;
        
        for (let i=0; i<scale.length; i++) {
            if (scale[i] == note) {
                return true;
            }
        }
        
        return false;
    }

    validNotesBetween(start, end) {
        let notesInScale = this.scale.notes;
        let valid = [];
        
        for (let i=(start + 1); i<end; i++) {
            if (this.noteInScale(i, notesInScale)) {
                valid.push(i);
            }
        }
        
        return valid;
    }
    
    pickChordNotes() {
        let chords = this.progression.chordNotes;
        let pickedNotes = [];
        
        for (let i=0; i<chords.length; i++) {
            let howMany = this.progression.duration[i] / this.chordNoteLength;
            
            if (!i) {
                pickedNotes.push(this.pickNote(chords[i]));
            } else {
                pickedNotes.push(this.pickNote(chords[i], pickedNotes[pickedNotes.length - 1]));
            }
            
            for (let j=0; j<(howMany - 1); j++) {
                pickedNotes.push(this.pickNote(chords[i], pickedNotes[pickedNotes.length - 1]));
            }
        }
        
        pickedNotes.push(this.pickNote(chords[0], pickedNotes[pickedNotes.length - 1]));
        return pickedNotes;
    }
    
    getNoteArray(pickedNotes) {
        let allNotes = [];
        
        for (let i=0; i<pickedNotes.length; i++) {
            let between = [];
            let direction;
            let low = 0;
            let high = 0;
            
            if (i == (pickedNotes.length - 1)) {
                break;
            }
            
            if (pickedNotes[i] >= pickedNotes[i + 1]) {
                direction = 'down';
                low = pickedNotes[i + 1];
                high = pickedNotes[i];
                
            } else {
                direction = 'up';
                low = pickedNotes[i];
                high = pickedNotes[i + 1];
            }
            
            between = this.validNotesBetween(low, high);
            
            while (between.length < 3) {
                if (direction == 'down') {
                    high++;
                } else {
                    low--;
                }
                
                between = this.validNotesBetween(low, high);
            }
            
            between = Util.getXRandomFromArray(between, this.notesBetween);
            
            if (direction == 'down') {
                between.sort(function(a, b) {
                    return b - a;
                });
            } else {
                between.sort(function(a, b) {
                    return a - b;
                });
                
            }
            
            allNotes.push(pickedNotes[i]);
            between.forEach(function(note) {
                allNotes.push(note);
            });
        }
        
        if (this.wrapLow) {
            for (let i=0; i<allNotes.length; i++) {
                while (allNotes[i] <= this.wrapLow) {
                    allNotes[i] += 12;
                }
            }
        }
        
        return allNotes;
    }
    
    generate() {
        if (typeof(this.wrapLow) == 'undefined') {
            this.wrapLow = 0;
        }
        
        let pickedNotes = this.pickChordNotes();
        this.notes = this.getNoteArray(pickedNotes);
        return this;
    }
}

module.exports = {
    create: function(scale, progression, chordNoteLength, notesBetween, rangeStart, rangeEnd, wrapLow) {
        let randomArray = new RandomNoteArray(scale, progression, chordNoteLength, notesBetween, rangeStart, rangeEnd, wrapLow);
        return randomArray.generate();
    }
};