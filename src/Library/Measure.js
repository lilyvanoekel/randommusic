"use strict";

class Measure {
    constructor() {
        this.repeatStart = false;
        this.repeatEnd = false;
        this.notes = [];
    }
    
    fixSyncopation() {
        let newNotes = [];
        let position = 0;
        
        for (let i=0; i<this.notes.length; i++) {
            let note = this.notes[i];
            let newDurations = [];
            
            // Syncopation/tie rules seems to be at least partially subjective. Unable to find any specific rules, only including very specific cases for now.
            switch(true) {
                case (position == 48 && note.duration == 192):
                    newDurations = [48, 144];
                    break;
                
                case (position == 48 && note.duration == 288):
                    newDurations = [48, 192, 48];
                    break;
            }
            
            if (newDurations.length) {
                newDurations.forEach((duration, index) => {
                    let newNote = note.copy();
                    newNote.duration = duration;
                    newNote.tie = (index ? true : false);
                    newNotes.push(newNote);
                });
            } else {
                newNotes.push(note);
            }
            
            position += note.duration;
        }
        
        this.notes = newNotes;
    }
}

module.exports = {
    create: function() {
        return new Measure();
    }
};