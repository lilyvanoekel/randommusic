const express = require('express');
const SightReading = require('./sight-reading.js');
const shortid = require('shortid');
const fs = require('fs');
const shell = require('shelljs');

let app = express();

app.get('/api', function (req, res) {
    let options = {
        'bpm': typeof(req.query.bpm) == 'undefined' ? 120 : req.query.bpm, 
        'structure': typeof(req.query.structure) == 'undefined' ? 'AABA' : req.query.structure, 
        'mode': typeof(req.query.mode) == 'undefined' ? 'major' : req.query.mode, 
        'key': typeof(req.query.key) == 'undefined' ? 'C' : req.query.key,
        'time': typeof(req.query.time) == 'undefined' ? '4' : req.query.time,
        'lessChordNotes': typeof(req.query.lessChordNotes) == 'undefined' || req.query.lessChordNotes == 'false' || req.query.lessChordNotes == '0' ? false : req.query.lessChordNotes,
    };
    
    let fileBase = shortid.generate();
    let pathBase = '/var/www/files/tmp/' + fileBase;
    let pathAbc = pathBase + '.abc';
    let pathPs = pathBase + '.ps';
    let pathSvg = pathBase + '.svg';
    let pathMid = pathBase + '.mid';
    let pathMp3 = pathBase + '.mp3';
    let pathPng = pathBase + '.png';
    let abcString = SightReading(options);
    
    fs.writeFileSync(pathAbc, abcString);
    //shell.exec('abcm2ps ' + pathAbc + ' -O ' + pathPs);
    shell.exec('abcm2ps ' + pathAbc + ' -g -O ' + pathSvg);
    shell.exec('abc2midi ' + pathAbc + ' -o ' + pathMid);
    shell.exec('timidity ' + pathMid + ' -Ow -o - | lame - -b 64 ' + pathMp3);
    //shell.exec('convert -density 150 -background white -alpha remove -trim -bordercolor white -border 20 ' + pathPs + ' ' + pathPng);
    
    let json = {
        'abc': escape(abcString),
        'midi': '/files/tmp/' + fileBase + '.mid',
        'mp3': '/files/tmp/' + fileBase + '.mp3',
        //'png': '/files/tmp/' + fileBase + '.png',
        'svg': '/files/tmp/' + fileBase + '001.svg'
    };
    
    res.send(json);
});

app.listen(4444, function () {
    console.log('Listening on port 4444');
});