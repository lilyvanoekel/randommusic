"use strict";

const Util = require('./Library/Util.js');
const Scale = require('./Library/Scale.js');
const SightReading = require('./sight-reading.js');
const fs = require('fs');
const shell = require('shelljs');
const Twitter = require('twitter');
require('dotenv').config();

const key = Util.arrayRandomChild(['C', 'C', 'C', 'D', 'E', 'F', 'G', 'G', 'G', 'A', 'A', 'A', 'B']);
const mode = Util.arrayRandomChild(['major', 'minor', 'major', 'minor', 'major', 'minor', 'mix']);
const scale = Scale.create(key, mode);

let options = {
    'bpm': 120, 
    'structure': Util.arrayRandomChild(['AABA', 'AA', 'AABA', 'AA', 'AABA']), 
    'mode': mode, 
    'key': key,
    'time': Util.arrayRandomChild(['4', '4', '3', '4']), // removed '3' for now, issue with dotted half note and eight note in same measure
    'lessChordNotes': Util.arrayRandomChild([false, false, false, true]),
    'title': 'Sight Reading Exercise - ' + scale.display
};

let argv = require('minimist')(process.argv.slice(2));

if (typeof(argv['p']) != 'undefined') {
    let path = argv['p'].replace(/\/?$/, '/');
    
    const countFile = 'sight-reading-count.txt';
    let count = 0;
    
    if (fs.existsSync(countFile)) {
        count = parseInt(fs.readFileSync(countFile), 10);
    }
    
    count++;
    fs.writeFileSync(countFile, count);
    
    options.title = 'Sight Reading Exercise ' + count + ' - ' + scale.display;
    
    const file = path + 'exercise-' + count;
    const fileAbc = file + '.abc';
    const filePs = file + '.ps';
    const fileMid = file + '.mid';
    const filePng = file + '.png';
    const fileHtml = file + '.html';
    const abcString = SightReading(options);
    const liveUrl = 'http://www.gimmesomethingtoplay.com/piano#' + escape(abcString);
    
    fs.writeFileSync(fileAbc, abcString);
    
    shell.exec('abcm2ps ' + fileAbc + ' -O ' + filePs);
    shell.exec('abc2midi ' + fileAbc + ' -o ' + fileMid);
    shell.exec('convert -density 150 -background white -alpha remove -trim -bordercolor white -border 20 ' + filePs + ' ' + filePng);
    
const htmlString = `<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="refresh" content="0; url=${liveUrl}">
        <script type="text/javascript">
            window.location.href = "${liveUrl}"
        </script>
        <title>Page Redirection</title>
    </head>
    <body>
        If you are not redirected automatically, follow this <a href='${liveUrl}'>link</a>.
    </body>
</html>`;

    fs.writeFileSync(fileHtml, htmlString);
    
    let client = new Twitter({
        consumer_key: process.env.TWITTER_CONSUMER_KEY,
        consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
        access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
        access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
    });
    
    if (fs.existsSync(filePng)) {
        let data = fs.readFileSync(filePng);

        // Make post request on media endpoint. Pass file data as media parameter
        client.post('media/upload', {media: data}, function(error, media, response) {
            if (error) {
                console.log(error);
                return;
            }
            
            // Lets tweet it
            let status = {
                status: 'http://www.gimmesomethingtoplay.com/files/sight-reading/exercise-' + count + '.html #sightreading #piano',
                media_ids: media.media_id_string // Pass the media id string
            }
            
            client.post('statuses/update', status, function(error, tweet, response) {
                if (error) {
                    console.log(error);
                    return;
                }
                
                console.log('Twitteted');
            });
        });
    }
    
    // convert -density 150 -background white -alpha remove -trim -bordercolor white -border 20 Out.ps exercise-2.png
} else {
    console.log(SightReading(options));
}
