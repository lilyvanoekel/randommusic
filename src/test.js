"use strict";

const Util = require('./Library/Util.js');
const Scale = require('./Library/Scale.js');
const SightReading = require('./sight-reading.js');

const key = Util.arrayRandomChild(['C', 'C', 'C', 'D', 'E', 'F', 'G', 'G', 'G', 'A', 'A', 'A', 'B']);
let mode = Util.arrayRandomChild(['major', 'minor', 'major', 'minor', 'major', 'minor', 'mix']);
const scale = Scale.create(key, mode);

mode = 'loc';

let options = {
    'bpm': 120, 
    'structure': Util.arrayRandomChild(['AABA', 'AA', 'AABA', 'AA', 'AABA']), 
    'mode': mode, 
    'key': key,
    'time': '4',
    'lessChordNotes': Util.arrayRandomChild([false, false, false, true]),
    'title': 'Sight Reading Exercise - ' + scale.display
};

const abcString = SightReading(options);
console.log(abcString);