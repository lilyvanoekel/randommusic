(function() {
    var render = function(abcString) {
        ABCJS.renderAbc('notation', abcString);
        ABCJS.renderMidi('midi', abcString, {
            generateDownload: true,
        });
        
        var link = document.getElementById('link');
        link.href = '#' + escape(abcString);
    };
    
    var load = function() {
        $('#notation').html('Loading...');
        $('#midi').html('');
        
        var speed = document.getElementById('speed');
        var structure = document.getElementById('structure');
        var mode = document.getElementById('mode');
        var key = document.getElementById('key');
        var time = document.getElementById('time');
        var lessChordNotes = document.getElementById('fewerChordNotes');
        var options = {
            bpm: speed.value,
            structure: structure.value,
            mode: mode.value,
            key: key.value,
            time: time.value,
            lessChordNotes: lessChordNotes.value
        };
        
        $.ajax({
            url: '/api',
            type: 'get',
            data: options,
            success: function(response) {
                processResponse(response);
            }
        });
    };
    
    var processResponse = function(json) {
        var textarea = document.getElementById('textarea');
        
        textarea.value = unescape(json.abc);
        $('#notation').html('<img src="' + json.svg + '">');
        $('#midi').html('<audio controls src="' + json.mp3 + '" controls="controls"></audio><br><a href="' + json.midi + '" target="_blank">Download Midi</a>');
    };
    
    window.onload = function() {
        load();
        
        var button = document.getElementById('new');
        button.onclick = function() {
            load();
        };
        
        return;
        var abcString = '';
        var hash = window.location.hash + '';
        var speed = document.getElementById('speed');
        var structure = document.getElementById('structure');
        var mode = document.getElementById('mode');
        var key = document.getElementById('key');
        var time = document.getElementById('time');
        var lessChordNotes = document.getElementById('lessChordNotes');
        var options = {
            bpm: speed.value,
            structure: structure.value,
            mode: mode.value,
            key: key.value,
            time: time.value,
            lessChordNotes: lessChordNotes.checked
        };
        
        var generateNew = function() {
            window.location.hash = '';
            options = {
                bpm: speed.value,
                structure: structure.value,
                mode: mode.value,
                key: key.value,
                time: time.value,
                lessChordNotes: lessChordNotes.checked
            };
            abcString = window.generateAbc(options);
            render(abcString);
            textarea.value = abcString;
        };
        
        if (hash.length > 100) {
            abcString = unescape(hash).replace(/^#/, '');
        } else {
            abcString = window.generateAbc(options);
        }
        
        render(abcString);
        
        var textarea = document.getElementById('textarea');
        textarea.value = abcString;
        textarea.onchange = function() {
            render(textarea.value);
        };
        
        var button = document.getElementById('new');
        button.onclick = function() {
            generateNew();
        };
        
        speed.onchange = function() {
            abcString = abcString.replace(/\nQ:\s?1\/4\s=\s\d+\n/, "\nQ:1/4 = " + speed.value + "\n");
            render(abcString);
            textarea.value = abcString;
        };
        
        structure.onchange = function() {
            generateNew();
        };
        
        mode.onchange = function() {
            generateNew();
        };
        
        key.onchange = function() {
            generateNew();
        };
        
        time.onchange = function() {
            generateNew();
        };
        
        lessChordNotes.onclick = function() {
            generateNew();
        };
        
        setInterval(function() {
            var a = document.getElementById('midi').getElementsByTagName('a');
            
            if (a.length) {
                a[0].target = '_blank';
            }
        }, 1000);
    };
})();