const chai = require('chai');
const expect = chai.expect;
const Measure = require('../../src/Library/Measure.js');
const Note = require('../../src/Library/Note.js');

describe('Library/Measure', function() {
    it('fixSyncopation leaves 4 quarter notes alone', function() {
        let measure = Measure.create();
        
        for (let i=0; i<4; i++) {
            let note = Note.create(36, 96);
            measure.notes.push(note);
        }
        
        measure.fixSyncopation();
        expect(measure.notes.length).to.equal(4);
    });
    
    it('fixSyncopation turns half note at position 48 into eighth note and dotted quarter', function() {
        let measure = Measure.create();
        
        let note1 = Note.create(36, 48);
        measure.notes.push(note1);
        
        let note2 = Note.create(36, 192);
        measure.notes.push(note2);
        
        let note3 = Note.create(36, 144);
        measure.notes.push(note3);
        
        measure.fixSyncopation();
        expect(measure.notes[0].duration).to.equal(48);
        expect(measure.notes[1].duration).to.equal(48);
        expect(measure.notes[2].duration).to.equal(144);
    });
    
    it('fixSyncopation turns dotted half note at position 48 into eighth note, half note and eighth note', function() {
        let measure = Measure.create();
        
        let note1 = Note.create(36, 48);
        measure.notes.push(note1);
        
        let note2 = Note.create(36, 288);
        measure.notes.push(note2);
        
        let note3 = Note.create(36, 48);
        measure.notes.push(note3);
        
        measure.fixSyncopation();
        expect(measure.notes[0].duration).to.equal(48);
        expect(measure.notes[1].duration).to.equal(48);
        expect(measure.notes[2].duration).to.equal(192);
        expect(measure.notes[1].duration).to.equal(48);
    });
});