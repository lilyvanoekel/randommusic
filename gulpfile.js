var gulp = require('gulp');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var babelify = require('babelify');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
const buffer = require("vinyl-buffer");

gulp.task('sight-reading', () => {
  'use strict';
  return browserify({entries: ["./src/sight-reading-page.js", "./src/sight-reading.js"]})
    .transform("babelify", {presets: ["es2015"]}).bundle()
    .on('error', onError)
    .pipe(source("sight-reading-min.js"))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('./piano'));
});

function onError(err){
  console.log(err);
  this.emit('end');
}

gulp.task('default', ['sight-reading']);