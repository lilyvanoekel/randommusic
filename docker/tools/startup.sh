#!/bin/sh
cd /var/www/randommusic
npm install
gulp

pm2 start /var/www/randommusic/src/sight-reading-api.js
cp /tools/nginx.conf /etc/nginx/nginx.conf
nginx -g 'daemon off;'