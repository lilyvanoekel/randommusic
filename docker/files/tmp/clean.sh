#!/bin/sh
find -maxdepth 1 -type f -name "*.mp3" -delete
find -maxdepth 1 -type f -name "*.abc" -delete
find -maxdepth 1 -type f -name "*.png" -delete
find -maxdepth 1 -type f -name "*.svg" -delete
find -maxdepth 1 -type f -name "*.mid" -delete
find -maxdepth 1 -type f -name "*.ps" -delete